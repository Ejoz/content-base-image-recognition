# Common imports
import numpy as np
import pandas as pd

# Configuration imports
from PIL import Image
from os import listdir
from os.path import isfile, join

# Model imports
from keras.applications import vgg16
from keras.preprocessing.image import load_img, img_to_array
from keras.applications.imagenet_utils import preprocess_input
from keras.models import Model
import keras.backend.tensorflow_backend as tb
tb._SYMBOLIC_SCOPE.value = True
from sklearn.metrics.pairwise import cosine_similarity

# Streamlit and app imports
import streamlit as st
import time
import resource


# Initialize the size of the images to fit into the model
# vgg_model = vgg16.VGG16(weights='imagenet')
# width = eval(str(vgg_model.layers[0].output.shape[1]))
# height = eval(str(vgg_model.layers[0].output.shape[2]))


def get_all_files(directory: str):
    """ Returns a list of all images from the directory given in argument. """
    return [(directory + "/" + f) for f in listdir(directory) if isfile(join(directory, f))]


def extract_image(file):
    """ Returns an image in PIL format to be understandable for the algorithm. """
    return load_img(file, target_size=(width, height))


def process_one_image(image_file):
    """ Turns an image into an array. """
    original = extract_image(image_file)
    numpy_image = img_to_array(original)
    return np.expand_dims(numpy_image, axis=0)


def process_images(directory):
    """ Returns a list of arrays extracted from the dataset of images. """
    files = get_all_files(directory)
    list_of_pics_array = []
    for f in files:
        image = process_one_image(f)
        list_of_pics_array.append(image)
    return np.vstack(list_of_pics_array)


def get_features(images):
    """ Extracts the features from the images. """
    dense_mat = preprocess_input(images)
    feat_extract = Model(inputs=vgg_model.input, outputs=vgg_model.layers[-2].output)
    return feat_extract.predict(dense_mat)


def get_similarity(img_features):
    """ Returns a pandas dataframe with the similarity between all images. """
    files = get_all_files('data')
    cos_similarities = cosine_similarity(img_features)
    return pd.DataFrame(cos_similarities, columns=files, index=files)


@st.cache(allow_output_mutation=True)
def train(directory):
    """ Returns the features from the images in the directory. """
    images = process_images(directory)
    return get_features(images)


def most_similar_to(input_img, n, sim_table):
    """ Returns a list of the n most similar bags. """
    closest_imgs = sim_table[input_img].sort_values(ascending=False)[1:n + 1].index
    closest_imgs_scores = sim_table[input_img].sort_values(ascending=False)[1:n + 1]

    most_similar_images = list()
    print("Input image :", input_img)
    print("Output images :")
    for i in range(0, len(closest_imgs)):
        most_similar_images.append(closest_imgs[i])
        print(closest_imgs[i], ', with a score of', closest_imgs_scores[i])

    return most_similar_images


# Training data
# trained_data = train('data')
# similarity_table = get_similarity(trained_data)

# # Transforming features into a dataframe for production
# df_similarity_table = get_similarity(trained_data)
# df_similarity_table.to_csv('/home/ejoz/Bureau/image-recognition-engine/similarity_table.csv', index=False)

similarity_table = pd.read_csv('similarity_table.csv', sep=',')
similarity_table['index'] = similarity_table.columns
similarity_table.set_index(similarity_table['index'], drop=True, inplace=True)
del similarity_table['index']


########################################################################################################################
# Running the app
########################################################################################################################


# Sidebar
files_names = get_all_files('data')
st.sidebar.info(
    """
    Cette application utilise l'extraction de *features* du modèle de réseau de neurones VGG-16.

    Développée par [Chloé Laurent](https://www.linkedin.com/in/chlolaurent/) avec [Heroku](https://www.heroku.com/) et [Streamlit](https://www.streamlit.io/).
    
    Le code est open source et disponible sur [GitLab](https://gitlab.com/Ejoz) sous la licence MIT.
    """
)

input_file = st.sidebar.selectbox('Choisissez une image parmi la liste ci-dessous.', files_names)
number_of_advices = st.sidebar.slider("Combien de sacs similaires désirez-vous voir ?", 0, 4, 1)

# Header
st.title("Content-Base Image Recognition (CBIR)")
st.markdown("Voir [*Recherche d'images par le contenu*]((https://fr.wikipedia.org/wiki/Recherche_d%27image_par_le_contenu)) sur Wikipédia")

# Displaying results
results = most_similar_to(input_file, number_of_advices, similarity_table)
st.sidebar.image(input_file, use_column_width=True)
for image in results:
    st.image(image, use_column_width=True)
